package com_pm_merchananchundiaronald.facci.aplicacion.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import com_pm_merchananchundiaronald.facci.aplicacion.DetalleActivity;
import com_pm_merchananchundiaronald.facci.aplicacion.Modelo.Administrativos;
import com_pm_merchananchundiaronald.facci.aplicacion.R;

public
class AdaptadorAdministrador {

    public static
    class AdaptadorAdministrativo extends RecyclerView.Adapter<AdaptadorAdministrativo.MyViewHolder> {

        private ArrayList<Administrativos> administrativosArrayList;
        private static final String URL_P = "http://10.1.15.127:3005/api/administrativos/";
        public AdaptadorAdministrativo(ArrayList<Administrativos> administrativosArrayList) {
            this.administrativosArrayList = administrativosArrayList;
        }

        @NonNull
        @Override
        public AdaptadorAdministrativo.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).
                    inflate(R.layout.vista_item_admin, viewGroup, false);

            return new MyViewHolder(view);

        }

        @Override
        public void onBindViewHolder(@NonNull AdaptadorAdministrativo.MyViewHolder myViewHolder, final int position) {

            final Administrativos administrativos = administrativosArrayList.get(position);
            myViewHolder.nombre.setText(administrativos.getNombre());
            Picasso.get().load(administrativos.getFoto()).into(myViewHolder.imageView);
            myViewHolder.view1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, DetalleActivity.class);
                    intent.putExtra("ronald", administrativos.getId().trim());
                    context.startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return administrativosArrayList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private View view1;
            private ImageView imageView;
            private TextView nombre;


            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                view1 = itemView;
                imageView = (ImageView)view1.findViewById(R.id.ImagenPLista);
                nombre = (TextView)view1.findViewById(R.id.LBLNom);


            }
        }
    }

}
