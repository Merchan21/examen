package com_pm_merchananchundiaronald.facci.aplicacion.Modelo;

public
class Administrativos {
    private String id, nombre, apellido, cargo, foto;

    public
    String getId() {
        return id;
    }

    public
    void setId(String id) {
        this.id = id;
    }

    public
    String getNombre() {
        return nombre;
    }

    public
    void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public
    String getApellido() {
        return apellido;
    }

    public
    void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public
    String getCargo() {
        return cargo;
    }

    public
    void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public
    String getFoto() {
        return foto;
    }

    public
    void setFoto(String foto) {
        this.foto = foto;
    }

    public
    Administrativos() {
    }
}
