package com_pm_merchananchundiaronald.facci.aplicacion;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import com_pm_merchananchundiaronald.facci.aplicacion.Adapter.AdaptadorAdministrador;
import com_pm_merchananchundiaronald.facci.aplicacion.Modelo.Administrativos;

public
class MainActivity extends AppCompatActivity {

    private static final String URL_P = "http://10.1.15.127:3005/api/administrativos/";
    private RecyclerView recyclerView;
    private ArrayList<Administrativos> administrativosArrayList;
    private ProgressDialog progressDialog;
    private AdaptadorAdministrador.AdaptadorAdministrativo adaptadorAdministrativo;

    @Override
    protected
    void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView)findViewById(R.id.RecyclerAdministrativo);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        administrativosArrayList = new ArrayList<>();
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Cargando");
        progressDialog.show();
        adaptadorAdministrativo = new AdaptadorAdministrador.AdaptadorAdministrativo(administrativosArrayList);
        DatosGenerales();
    }

    private void DatosGenerales() {

        StringRequest stringRequest = new StringRequest(
                Request.Method.GET, URL_P, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //repuesta de que todo esta bien y me trae datos
                progressDialog.dismiss();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i<jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        Administrativos administrativos = new Administrativos();
                        administrativos.setNombre(jsonObject.getString("nombre"));
                        administrativos.setApellido(jsonObject.getString("Apellido"));
                        administrativos.setCargo(jsonObject.getString("cargo"));
                        administrativos.setFoto(jsonObject.getString("foto"));
                        administrativos.setId(jsonObject.getString("id"));

                        if (administrativos.getCargo().equals("true")){
                            administrativosArrayList.add(administrativos);
                        }
                    }
                    recyclerView.setAdapter(adaptadorAdministrativo);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // repuesta de que ocurrio un error mientras se hacia la petición
                Toast.makeText(MainActivity.this, "ERROR", Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}

